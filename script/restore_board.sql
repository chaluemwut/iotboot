drop table iot_blog, iot_blog_answer, blog_status, iot_users, users;
drop table iot_blog_backup, iot_blog_answer_backup, blog_status_backup, iot_users_backup, users_backup
create table iot_blog as select * from iot_blog_backup;
create table iot_blog_answer as select * from iot_blog_answer_backup;	
create table blog_status as select * from blog_status_backup;
create table iot_users as select * from iot_users_backup;
create table users as select * from users_backup;