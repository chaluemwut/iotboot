LOCK TABLES `acls` WRITE;
/*!40000 ALTER TABLE `acls` DISABLE KEYS */;
INSERT INTO `acls` VALUES (1,1,'loc/jjolie','jjolie'),(2,1,'loc/ro','jjolie'),(3,2,'loc/rw','jjolie'),(4,1,'$SYS/something','jjolie'),(5,1,'loc/test/#','a'),(6,1,'$SYS/broker/log/+','a'),(7,1,'mega/secret','su1'),(8,1,'mega/secret','nop'),(9,1,'loc/#','jog'),(10,1,'loc/#','m1'),(11,1,'x','ps1'),(12,1,'blabla/%c/priv/#','ps1');
/*!40000 ALTER TABLE `acls` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `blog_status` WRITE;
/*!40000 ALTER TABLE `blog_status` DISABLE KEYS */;
INSERT INTO `blog_status` VALUES (1,'draft'),(2,'publish');
/*!40000 ALTER TABLE `blog_status` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `iot_blog` WRITE;
/*!40000 ALTER TABLE `iot_blog` DISABLE KEYS */;
INSERT INTO `iot_blog` VALUES (1,'2017-10-31 18:34:10','&nbsp; &nbsp; iotboot  smartphone  node mcu   mqtt coap  m2m&nbsp; (Machine-2-Machine)     node mcu  web server  database server  internet <div><div>&nbsp; &nbsp; iotboot  2  mqtt  websocket<br></div></div>','iotboot platform',2,2),(2,'2017-10-31 18:34:34','<ul><li>password </li><li>indent  blog text editor</li><li> post</li><li>High light code (ok)</li></ul>','bug',1,2),(3,'2017-11-01 03:40:18','&nbsp; &nbsp;   serial port  node mcu',' Serial port  node mcu',1,2),(4,'2017-11-01 15:15:12','<div><br></div><div><pre style=\"background-color:#ffffff;color:#000000;font-family:\'Menlo\';font-size:9.0pt;\">#include &lt;<span style=\"color:#20999d;\">ESP8266WiFi</span>.h&gt;<br>#include &lt;<span style=\"color:#20999d;\">ESP8266HTTPClient</span>.h&gt;<br><br><span style=\"color:#000080;font-weight:bold;\">char</span>* ssid = <span style=\"color:#008000;font-weight:bold;\">\"...wifi name...\"</span>;<br><span style=\"color:#000080;font-weight:bold;\">char</span>* password = <span style=\"color:#008000;font-weight:bold;\">\"...wifi password...\"</span>;<br>WiFiServer server(<span style=\"color:#0000ff;\">80</span>);<br>WiFiClient client;<br><br><span style=\"color:#000080;font-weight:bold;\">void </span>setup() {<br>Serial.begin(<span style=\"color:#0000ff;\">9600</span>);<br>Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"Begin program\"</span>);<br>WiFi.begin(ssid, password);<br><span style=\"color:#000080;font-weight:bold;\">while</span>(WiFi.status() != WL_CONNECTED) {<br>delay(<span style=\"color:#0000ff;\">500</span>);<br>Serial.print(<span style=\"color:#008000;font-weight:bold;\">\".\"</span>);<br>}<br>Serial.println(WiFi.localIP());<br>}<br><br><span style=\"color:#000080;font-weight:bold;\">void </span>loop() {<br><span style=\"color:#000080;font-weight:bold;\">if </span>(WiFi.status() == WL_CONNECTED) { <span style=\"color:#808080;font-style:italic;\">//Check WiFi connection status<br></span>HTTPClient http;  <span style=\"color:#808080;font-style:italic;\">//Declare an object of class HTTPClient<br></span>http.begin(<span style=\"color:#008000;font-weight:bold;\">\"http://jsonplaceholder.typicode.com/users/1\"</span>);  <span style=\"color:#808080;font-style:italic;\">//Specify request destination<br></span><span style=\"color:#000080;font-weight:bold;\">int </span>httpCode = http.GET();                                                                  <span style=\"color:#808080;font-style:italic;\">//Send the request<br></span>Serial.print(httpCode);<br><span style=\"color:#000080;font-weight:bold;\">if </span>(httpCode &gt; <span style=\"color:#0000ff;\">0</span>) { <span style=\"color:#808080;font-style:italic;\">//Check the returning code<br></span>String payload = http.getString();   <span style=\"color:#808080;font-style:italic;\">//Get the request response payload<br></span>Serial.println(payload);                     <span style=\"color:#808080;font-style:italic;\">//Print the response payload<br></span>}<br>http.end();   <span style=\"color:#808080;font-style:italic;\">//Close connection<br></span>}<br><br>delay(<span style=\"color:#0000ff;\">30000</span>);    <span style=\"color:#808080;font-style:italic;\">//Send a request every 30 seconds<br></span>}</pre></div>','Node MCU read web page',1,2),(5,'2017-11-02 02:29:55','&nbsp; &nbsp; MQTT (<span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;Nimbus Sans L&quot;, Arial, &quot;Liberation Sans&quot;, sans-serif; font-size: 16px; vertical-align: baseline; background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(34, 34, 34);\">Message Queue Telemetry Transpor</span><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;Nimbus Sans L&quot;, Arial, &quot;Liberation Sans&quot;, sans-serif; font-size: 16px; vertical-align: baseline; background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(34, 34, 34);\">t)  protocol  iot  m2m  MQTT  publish  subscribe  broker  publish  topic  topic  broker  broker </span><div><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;Nimbus Sans L&quot;, Arial, &quot;Liberation Sans&quot;, sans-serif; font-size: 16px; vertical-align: baseline; background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(34, 34, 34);\">&nbsp; &nbsp; MQTT  TCP/IP protocol</span></div>','MQTT',1,2),(6,'2017-11-02 02:30:13','','Web socket',1,2);
/*!40000 ALTER TABLE `iot_blog` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `iot_blog_answer` WRITE;
/*!40000 ALTER TABLE `iot_blog_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `iot_blog_answer` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `iot_blog_backup` WRITE;
/*!40000 ALTER TABLE `iot_blog_backup` DISABLE KEYS */;
INSERT INTO `iot_blog_backup` VALUES (1,'2017-11-01 15:41:14','&nbsp; &nbsp; iotboot เป็นเครื่องมือเพื่อช่วยให้อุปกรณ์ต่างๆเช่น smartphone และ node mcu สามารถเชื่อมต่อกันได้ ซึ่งปัจจุบันมีรูปแบบที่ได้รับความนิยมเช่น mqtt coap เป็นต้น m2m&nbsp; (Machine-2-Machine) เป็นรูปแบบการส่งคำสั่งหรือข้อมูลจากเครื่องหนึ่งไปอีกเครื่องหนึ่งได้โดยตรง ซึ่งสามารถสั่งงานอุปกรณ์ขนาดเล็กด้วยมือถือหรือเครื่องคอมพิวเตอร์ หรือสามารถเก็บข้อมูลจากอุปกรณ์ขนาดเล็กเพื่อนำเสนอกับผู้ใช้งาน อุปกรณ์ขนาดเล็กเช่น node mcu ไม่เหมาะสำหรับการติดตั้งโปรแกรมขนาดใหญ่เช่น web server และ database server ดังนั้นการส่งข้อมูลขึ้นมายัง internet จึงเป็นวิธีการที่ได้รับความนิยมสำหรับการควบคุมอุปกรณ์ต่างๆผ่านมือถือ<div>&nbsp; &nbsp; iotboot สนับสนุน 2 รูปแบบการเชื่อมต่อคือ mqtt และ websocket</div>','pppp',2,1),(2,'2017-11-01 16:30:00','java code<div><br></div><div><div><pre style=\"background-color:#ffffff;color:#000000;font-family:\'Menlo\';font-size:9.0pt;\"><span style=\"color:#808000;\">@SpringBootApplication<br></span><span style=\"color:#000080;font-weight:bold;\">public class </span>Application {<br><br>   <span style=\"color:#000080;font-weight:bold;\">public static void </span>main(String[] args) {<br>      SpringApplication.<span style=\"font-style:italic;\">run</span>(Application.<span style=\"color:#000080;font-weight:bold;\">class</span>, args);<br>   }<br>}<br></pre></div></div>','test code',2,1),(3,'2017-11-03 10:52:11','<div><br></div><div><br></div><div><br></div><div><pre style=\"background-color:#ffffff;color:#000000;font-family:\'Menlo\';font-size:9.0pt;\">        #include &lt;<span style=\"color:#20999d;\">ESP8266WiFi</span>.h&gt;<br>        #include &lt;<span style=\"color:#20999d;\">WiFiClientSecure</span>.h&gt;<br><br>        <span style=\"color:#000080;font-weight:bold;\">char</span>* ssid = <span style=\"color:#008000;font-weight:bold;\">\"Sinet_home_wifi\"</span>;<br>        <span style=\"color:#000080;font-weight:bold;\">char</span>* password = <span style=\"color:#008000;font-weight:bold;\">\"aabbccbbaa\"</span>;<br><br>        <span style=\"color:#000080;font-weight:bold;\">const char</span>* host = <span style=\"color:#008000;font-weight:bold;\">\"iotboot.com\"</span>;<br>        <span style=\"color:#000080;font-weight:bold;\">const int </span>httpsPort = <span style=\"color:#0000ff;\">443</span>;<br><br><span style=\"color:#808080;font-style:italic;\">// Use web browser to view and copy<br></span><span style=\"color:#808080;font-style:italic;\">// SHA1 fingerprint of the certificate<br></span><span style=\"color:#808080;font-style:italic;\">        </span><span style=\"color:#000080;font-weight:bold;\">const char</span>* fingerprint = <span style=\"color:#008000;font-weight:bold;\">\"48 F4 79 4E 5E 00 CC 09 77 85 34 0A 5F 09 FA AC CE 69 D4 00\"</span>;<br><br>        WiFiClientSecure client;<br><br>        <span style=\"color:#000080;font-weight:bold;\">void </span>setup() {<br>        Serial.begin(<span style=\"color:#0000ff;\">115200</span>);<br>        WiFi.begin(ssid, password);<br>        <span style=\"color:#000080;font-weight:bold;\">while </span>(WiFi.status() != WL_CONNECTED) {<br>        delay(<span style=\"color:#0000ff;\">500</span>);<br>        Serial.print(<span style=\"color:#008000;font-weight:bold;\">\".\"</span>);<br>        }<br>        Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"\"</span>);<br>        Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"WiFi connected\"</span>);<br>        Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"IP address: \"</span>);<br>        Serial.println(WiFi.localIP());<br><br>        <span style=\"color:#808080;font-style:italic;\">// Use WiFiClientSecure class to create TLS connection<br></span><span style=\"color:#808080;font-style:italic;\">        </span>Serial.print(<span style=\"color:#008000;font-weight:bold;\">\"connecting to \"</span>);<br>        Serial.println(host);<br>        <span style=\"color:#000080;font-weight:bold;\">if </span>(!client.connect(host, httpsPort)) {<br>        Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"connection failed\"</span>);<br>        <span style=\"color:#000080;font-weight:bold;\">return</span>;<br>        }<br>        client.print(String(<span style=\"color:#008000;font-weight:bold;\">\"GET \"</span>) + <span style=\"color:#008000;font-weight:bold;\">\"/blog/view?id=1 HTTP/1.1</span><span style=\"color:#000080;font-weight:bold;\">\\r\\n</span><span style=\"color:#008000;font-weight:bold;\">\" </span>+<br>        <span style=\"color:#008000;font-weight:bold;\">\"Host: iotboot.com</span><span style=\"color:#000080;font-weight:bold;\">\\r\\n</span><span style=\"color:#008000;font-weight:bold;\">\" </span>+<br>        <span style=\"color:#008000;font-weight:bold;\">\"Connection: close</span><span style=\"color:#000080;font-weight:bold;\">\\r\\n\\r\\n</span><span style=\"color:#008000;font-weight:bold;\">\"</span>);<br><br><br>        Serial.println(<span style=\"color:#008000;font-weight:bold;\">\"request sent\"</span>);<br>        }<br><br>        <span style=\"color:#000080;font-weight:bold;\">void </span>loop() {<br>        <span style=\"color:#000080;font-weight:bold;\">if </span>(client.available()) {<br>        <span style=\"color:#000080;font-weight:bold;\">char </span>c = client.read();<br>        Serial.print(c);<br>        }<br>        }</pre></div>','Node MCU read https',1,1);
/*!40000 ALTER TABLE `iot_blog_backup` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `iot_users` WRITE;
/*!40000 ALTER TABLE `iot_users` DISABLE KEYS */;
INSERT INTO `iot_users` VALUES (1,'2017-11-01 01:33:31','d13GYtj0nEA=',NULL,7),(2,'2017-10-31 18:33:51','d13GYtj0nEA=',NULL,8);
/*!40000 ALTER TABLE `iot_users` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `mqtt_data` WRITE;
/*!40000 ALTER TABLE `mqtt_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `mqtt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'PBKDF2$sha256$901$pN94c3+KCcNvIV1v$LWEyzG6v/gtvTrjx551sNcWWfwIZKAg0',0,'jjolie'),(2,'PBKDF2$sha256$901$XPkOwNbd05p5XsUn$1uPtR6hMKBedWE44nqdVg+2NPKvyGst8',0,'a'),(3,'PBKDF2$sha256$901$chEZ4HcSmKtlV0kf$yRh2N62uq6cHoAB6FIrxIN2iihYqNIJp',1,'su1'),(4,'PBKDF2$sha256$901$sdMgoJD3GaRlTF7y$D7Krjx14Wk745bH36KBzVwHwRQg0a+z6',1,'S1'),(5,'PBKDF2$sha256$2$NLu+mJ3GwOpS7JLk$eITPuWG/+WMf6F3bhsT5YlYPY6MmJHvM',0,'m1'),(6,'deaddead',0,'ps1'),(7,'PBKDF2$sha256$901$YTuoUq33GlEZ2zRM$2YKM336mCtlDTGagoq2JG04j1zGKAise',1,'off'),(8,'PBKDF2$sha256$901$RK0fUxsCXAisOZYc$KmgR1oWwHEDkZsXHbcr9dHGZi/bPRZn+',1,'chaluemwut');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `ws_message` WRITE;
/*!40000 ALTER TABLE `ws_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ws_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ws_receiver`
--

LOCK TABLES `ws_receiver` WRITE;
/*!40000 ALTER TABLE `ws_receiver` DISABLE KEYS */;
/*!40000 ALTER TABLE `ws_receiver` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `ws_topic` WRITE;
/*!40000 ALTER TABLE `ws_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `ws_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ws_topic_ws_receivers`
--

LOCK TABLES `ws_topic_ws_receivers` WRITE;
/*!40000 ALTER TABLE `ws_topic_ws_receivers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ws_topic_ws_receivers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-08 15:50:43
