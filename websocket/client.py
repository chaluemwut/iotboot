import websocket
import _thread as thread
import time

def on_message(ws, message):
    print(message)

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://localhost:5000",
                              on_message = on_message)
    ws.run_forever()