from websocket_server import WebsocketServer
import requests, os, pickle, json

client_lst = {}

def new_client(client, server):
	print(client)
	server.send_message_to_all("Hey all, a new client has joined us")

def message_received(client, server, message):
    try:
        client_id = client['address'][1]
        message_lst = message.split(':')
        cmd = message_lst[0]
        if cmd == 'connect':
            url = '{}/connect?client_id={}&username={}&password={}'.format('http://app:8080/dbws',
                                                                           client_id,
                                                                           message_lst[1],
                                                                           message_lst[2])
            api_result = requests.get(url)
            client_lst[str(client_id)] = client
            print(client_lst)
            # pickle.dump(client, open('client_data/{}'.format(client_id), 'w'))
        elif cmd == 'push':
            # http://localhost:8080/dbws/push?client_id=1234&topic=room1&message=plplaas
            topic = message_lst[1]
            ws_message = message_lst[2]
            url = '{}/push?client_id={}&topic={}&message={}'.format('http://app:8080/dbws',
                                                                    client_id,
                                                                    topic,
                                                                    ws_message)
            api_result = requests.get(url)
            url = '{}/receive/list?topic={}'.format('http://app:8080/dbws', topic)
            receive_obj = requests.get(url)
            receive_res = json.loads(receive_obj.text)
            rec_lst = receive_res['data']['receive']
            for rec_id in rec_lst:
                try:
                    client_obj = client_lst[rec_id]
                    server.send_message(client_obj, ws_message)
                except Exception as e:
                    print(str(e))
        elif cmd == 'receive':
            topic = message_lst[1]
            url = '{}/receive/add?client_id={}&topic={}'.format('http://app:8080/dbws',
                                                         client_id,
                                                         topic)
            api_result = requests.get(url)

        server.send_message(client, 'test')
    except Exception as e:
        print(str(e))

server = WebsocketServer(5000, host='0.0.0.0')
server.set_fn_new_client(new_client)
server.set_fn_message_received(message_received)
server.run_forever()

# server
# connect:username:password
# push:topic:on

# client
# connect:username:password
# receive:topic

#### dev spec

# connect:client_id:username:password
# push:client_id:topic:message

