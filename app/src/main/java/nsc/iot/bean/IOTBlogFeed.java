package nsc.iot.bean;

import nsc.iot.model.table.IOTBlog;

public class IOTBlogFeed {
    private IOTBlog iotBlog;
    private String titleImage;

    public IOTBlog getIotBlog() {
        return iotBlog;
    }

    public void setIotBlog(IOTBlog iotBlog) {
        this.iotBlog = iotBlog;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }
}
