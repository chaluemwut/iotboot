package nsc.iot.bean;

public class MQTTBean {
    private String iotUserId;
    private String token;
    private String userName;

    public String getIotUserId() {
        return iotUserId;
    }

    public void setIotUserId(String iotUserId) {
        this.iotUserId = iotUserId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "MQTTBean{" +
                "iotUserId='" + iotUserId + '\'' +
                ", token='" + token + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
