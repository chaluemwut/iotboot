package nsc.iot.gateway.db;

import nsc.iot.bean.APIResponse;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.service.WSMessageRespository;
import nsc.iot.model.service.WSReceiverRespository;
import nsc.iot.model.service.WSTopicRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.WSMessage;
import nsc.iot.model.table.WSReceiver;
import nsc.iot.model.table.WSTopic;
import nsc.iot.sec.IOTEncrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/dbws")
public class DbWebSocketGateway {
    @Autowired
    private IOTUserRespository iotUserRespository;

    @Autowired
    private IOTEncrypt iotEncrypt;

    @Autowired
    private WSTopicRespository wsTopicRespository;

    @Autowired
    private WSMessageRespository wsMessageRespository;

    @Autowired
    private WSReceiverRespository wsReceiverRespository;

    @RequestMapping(value = "/connect", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public APIResponse connect(@RequestParam("client_id") String clientId,
                               @RequestParam("username") String username,
                               @RequestParam("password") String password) {
        APIResponse res = new APIResponse();
        IOTUser iotUser = null;//iotUserRespository.findByUserName(username);
        if (iotUser == null ||
                iotUser.getToken() == null ||
                !iotEncrypt.decrypt(iotUser.getToken()).equals(password)) {
            res.setStatus(1);
            res.setMessage("No users");
        } else {
            iotUser.setWsClientId(clientId);
            iotUserRespository.save(iotUser);
            res.setStatus(0);
        }
        return res;
    }

    @RequestMapping(value = "/push", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public APIResponse push(@RequestParam("client_id") String clientId,
                            @RequestParam("topic") String topic,
                            @RequestParam("message") String message) {
        APIResponse res = new APIResponse();
        WSTopic wsTopic = wsTopicRespository.findByTopic(topic);
        IOTUser iotUser = null;//iotUserRespository.findByWsClientId(clientId);
        if (iotUser == null) {
            iotUser = new IOTUser();
            iotUser.setWsClientId(clientId);
            iotUser = iotUserRespository.save(iotUser);
        }

        if (wsTopic == null) {
            wsTopic = new WSTopic();
            wsTopic.setTopic(topic);
            wsTopic.setPublisher(iotUser);
            wsTopic = wsTopicRespository.save(wsTopic);
        }
        WSMessage wsMessage = new WSMessage();
        wsMessage.setWsTopic(wsTopic);
        wsMessage.setMessage(message);
        wsMessage.setIotUser(iotUser);
        wsMessage = wsMessageRespository.save(wsMessage);
        return res;
    }

    @RequestMapping(value = "/receive/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public APIResponse receiveAdd(@RequestParam("client_id") String clientId,
                                  @RequestParam("topic") String topic) {
        APIResponse res = new APIResponse();
        IOTUser iotUser = null;//iotUserRespository.findByWsClientId(clientId);
        if (iotUser == null) {
            res.setStatus(1);
            res.setMessage("No client id");
            return res;
        }
        WSReceiver wsReceiver = wsReceiverRespository.findByClientId(clientId);
        if (wsReceiver == null) {
            wsReceiver = new WSReceiver();
        }

        wsReceiver.setClientId(clientId);
        wsReceiver.setIotUser(iotUser);
        WSTopic wsTopic = wsTopicRespository.findByTopic(topic);
        Set<WSTopic> topicList = wsReceiver.getWsTopicSet();
        if (topicList == null) {
            topicList = new HashSet<>();
        }
        topicList.add(wsTopic);
        wsReceiver = wsReceiverRespository.save(wsReceiver);

        if (wsTopic == null) {
            wsTopic = new WSTopic();
            Set<WSReceiver> wsReceivers = new LinkedHashSet<>();
            wsTopic.setWsReceivers(wsReceivers);
            wsTopic.setTopic(topic);
        }

        wsTopic.getWsReceivers().add(wsReceiver);
        wsTopicRespository.save(wsTopic);

        return res;
    }

    @RequestMapping(value = "/receive/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public APIResponse receiveList(@RequestParam("topic") String topic) {
        APIResponse res = new APIResponse();
        Map<String, Object> retData = new HashMap<>();
        WSTopic wsTopic = wsTopicRespository.findByTopic(topic);
        if (wsTopic == null) {
            wsTopic = new WSTopic();
            wsTopic.setTopic(topic);
            wsTopic = wsTopicRespository.save(wsTopic);
        }

        List<String> reciverClientIdList = new ArrayList<>();

        Set<WSReceiver> receiver = wsTopic.getWsReceivers();

        if(receiver == null) {
            receiver = new LinkedHashSet<>();
        }
        receiver.forEach(e -> {
            reciverClientIdList.add(e.getClientId());
        });
        retData.put("receive", reciverClientIdList);

        Set<WSMessage> messages = wsTopic.getWsMessage();
        Comparator<WSMessage> comp = (e1, e2) -> {
            if (e1.getId() > e2.getId()) {
                return 1;
            } else if (e1.getId() < e2.getId()) {
                return -1;
            }
            return 0;
        };

        WSMessage maxMessage = messages.stream().max(comp).get();
        retData.put("message", maxMessage.getMessage());
        res.setStatus(1);
        res.setData(retData);
        return res;
    }

}
