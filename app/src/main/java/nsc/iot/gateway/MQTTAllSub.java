package nsc.iot.gateway;

import nsc.iot.bean.MQTTBean;
import nsc.iot.cache.SpringCache;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.service.MQTTDataRespository;
import nsc.iot.model.service.UserProfileRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.MQTTData;
import nsc.iot.model.table.UserProfile;
import nsc.iot.mqtt.MQTTMessage;
import nsc.iot.sec.IOTEncrypt;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistable;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public class MQTTAllSub implements Gateway {

    @Autowired
    private UserProfileRespository userProfileRespository;

    @Autowired
    private IOTUserRespository iotUserRespository;

    @Autowired
    private IOTEncrypt iotEncrypt;

    @Autowired
    private MQTTDataRespository mqttDataRespository;

    @Autowired
    private SpringCache springCache;

    @Override
    public void start() {
        Iterable<Object[]> iotUserList = iotUserRespository.findAllIotUser();
        Iterator<Object[]> itUser = iotUserList.iterator();
        while (itUser.hasNext()){
            Object[] iotUser = itUser.next();
            mqttSubTopic(iotUser);
        }
    }

    public void mqttSubTopic(Object[] iotUser) {
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            String iotUserId = iotUser[0].toString();
            String token = (String) iotUser[1];
            String userName = (String)iotUser[2];
            //it will be object
            MQTTBean bean = new MQTTBean();
            bean.setIotUserId(iotUserId);
            bean.setToken(token);
            bean.setUserName(userName);
            System.out.println("MQTT all sub ");
            System.out.println("bean "+bean.toString());
            MQTTMessage mqttMessage = new MQTTMessage(mqttDataRespository, springCache, bean);
            MqttClient client = new MqttClient("tcp://iotboot.com:1883", "aaa", persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName(userName);
            connOpts.setPassword(iotEncrypt.decrypt(token).toCharArray());
            client.connect(connOpts);
            client.subscribe("#");
            client.setCallback(mqttMessage);
            bean = null;
        } catch (MqttException mqttError) {
            mqttError.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

}
