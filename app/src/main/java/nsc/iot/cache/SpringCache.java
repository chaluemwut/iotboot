package nsc.iot.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
public class SpringCache {
    @Autowired
    private CacheManager cacheManager;

    public void clear(){
        System.out.println("begin clear");
        cacheManager.getCacheNames().parallelStream().forEach(name -> {
            System.out.println(name);
            cacheManager.getCache(name).clear();
        });
        System.out.println("end clear");
    }
}
