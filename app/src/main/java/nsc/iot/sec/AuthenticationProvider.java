package nsc.iot.sec;

import nsc.iot.Config;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.service.UserProfileRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class AuthenticationProvider implements org.springframework.security.authentication.AuthenticationProvider {

    @Autowired
    private HttpSession session;

    @Autowired
    private PBKDF2PasswordEncoder passwordEncoder;

    @Autowired
    private UserProfileRespository userProfileRespository;

    @Autowired
    private IOTUserRespository iotUserRespository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName().trim();
        String passWord = authentication.getCredentials().toString().trim();

        IOTUser iotUser = iotUserRespository.findIoTUserByUserName(userName);
        String pw = iotUserRespository.findPWByUserName(userName);

        if (pw != null && passwordEncoder.matches(passWord, pw)) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
            Authentication auth = new UsernamePasswordAuthenticationToken(userName, passWord, grantedAuths);
            System.out.println("start save session : "+iotUser);
            session.setAttribute(Config.SESSION_USERNAME_KEY, iotUser);
            return auth;
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}