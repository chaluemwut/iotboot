package nsc.iot.sec;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

@Component
public class PBKDF2PasswordEncoder implements PasswordEncoder {
    private static final int KEY_LENGTH = 24 * 8;
    private static final int SALT_LENGTH = 12;
    private static final int ITERATIONS = 901;

    @Override
    public String encode(CharSequence charSequence) {
        byte someBytes[] = new byte[PBKDF2PasswordEncoder.SALT_LENGTH];
        Random randomGenerator = new Random();
        randomGenerator.nextBytes(someBytes);
        String encodedSalt = Base64.getEncoder().encodeToString(someBytes);

        SecretKeyFactory f = null;
        try {
            f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(PBKDF2PasswordEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        KeySpec ks = new PBEKeySpec(charSequence.toString().toCharArray(), encodedSalt.getBytes(), PBKDF2PasswordEncoder.ITERATIONS, PBKDF2PasswordEncoder.KEY_LENGTH);
        SecretKey s;
        try {
            s = f.generateSecret(ks);
            String encodedKey = Base64.getEncoder().encodeToString(s.getEncoded());

            byte[] bdecode = Base64.getDecoder().decode(encodedKey);
            String strdecode = new String(bdecode);

            String hashedKey = "PBKDF2$sha256$" + PBKDF2PasswordEncoder.ITERATIONS + "$" + encodedSalt + "$" + encodedKey;
            return hashedKey;
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(PBKDF2PasswordEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @Override
    public boolean matches(CharSequence plainPassword, String hashedPasword) {
        String[] encodedPassword = hashedPasword.split("\\$");
        int encodedIterations = Integer.parseInt(encodedPassword[2]);
        byte[] encodedSalt = encodedPassword[3].getBytes(Charset.forName("UTF-8"));
        String encodedHash = encodedPassword[4];
        SecretKeyFactory f = null;
        try {
            f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Need a Java implementation with cryptography.");
        }
        KeySpec ks = new PBEKeySpec(plainPassword.toString().toCharArray(), encodedSalt, encodedIterations, 198);
        SecretKey s = null;
        try {
            s = f.generateSecret(ks);
        } catch (InvalidKeySpecException e) {
            System.out.println("Encoded password is corrupt.");
        }
        return encodedHash.equals(Base64.getEncoder().encodeToString(s.getEncoded()));
    }

    public static void main(String [] args){
        PBKDF2PasswordEncoder obj = new PBKDF2PasswordEncoder();
        String str = obj.encode("1234");
        boolean b = obj.matches("1234", str);
        System.out.println(b);
        System.out.println(str);
    }
}
