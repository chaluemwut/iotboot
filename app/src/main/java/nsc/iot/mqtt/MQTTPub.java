package nsc.iot.mqtt;

import nsc.iot.Config;
import nsc.iot.cache.SpringCache;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.UserProfile;
import nsc.iot.sec.IOTEncrypt;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

@Component
public class MQTTPub {

    @Autowired
    HttpSession session;

    @Autowired
    IOTEncrypt iotEncrypt;

    @Autowired
    SpringCache springCache;

    @Autowired
    private IOTUserRespository iotUserRespository;

    public void pub(String topic, String message) {
        System.out.println("******* start MQTT pub *********");
        IOTUser iotUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
        System.out.println(iotUser);
        String userName = iotUserRespository.findUserName(iotUser.getId());
        System.out.println("user name : "+userName);
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            MqttClient client = new MqttClient("tcp://iotboot.com:1883", "bbb", persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName(userName);
            String token = iotEncrypt.decrypt(iotUser.getToken());
            connOpts.setPassword(token.toCharArray());
            client.connect(connOpts);

            MqttMessage mqttMessage = new MqttMessage(message.getBytes());
            mqttMessage.setQos(2);
            client.publish(topic, mqttMessage);
            client.disconnect();
        } catch (MqttException me) {
            me.printStackTrace();
        }
        springCache.clear();
        System.out.println("********* end MQTT pub ************");
    }
}
