package nsc.iot.mqtt;

import nsc.iot.bean.MQTTBean;
import nsc.iot.cache.SpringCache;
import nsc.iot.model.service.MQTTDataRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.MQTTData;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

//@Service
public class MQTTMessage implements MqttCallback {

    private MQTTBean bean;
    private MQTTDataRespository mqttDataRespository;
    private SpringCache springCache;
//
//    @Autowired
//    private MQTTDataRespository mqttDataRespository;
//
//    @Autowired
//    private SpringCache springCache;

    public MQTTMessage(MQTTDataRespository mqttDataRespository, SpringCache springCache, MQTTBean bean){
        this.mqttDataRespository = mqttDataRespository;
        this.springCache = springCache;
        this.bean = bean;
    }

//    public MQTTBean getBean() {
//        return bean;
//    }
//
//    public void setBean(MQTTBean bean) {
//        this.bean = bean;
//    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.out.println("start connection...");
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println("Start MQTT..."+bean);
        MQTTData mqttData = new MQTTData();
        mqttData.setMessage(new String(mqttMessage.getPayload()));
        mqttData.setTopic(s);
        mqttData.setIotUserId(Integer.parseInt(bean.getIotUserId()));
        mqttDataRespository.save(mqttData);
        springCache.clear();
        System.out.println("End MQTT message service.");
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        System.out.println("**** deliveryComplete");
//        springCache.clear();
    }
}
