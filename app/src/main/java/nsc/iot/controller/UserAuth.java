package nsc.iot.controller;

import nsc.iot.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/userauth")
public class UserAuth {

    @Autowired
    private HttpSession session;

    @RequestMapping("/signin")
    public ModelAndView signIn() {
        ModelAndView view = new ModelAndView("redirect:/console");
//        if (session.getAttribute(Config.SESSION_USERNAME_KEY) != null) {
//            view = new ModelAndView("redirect:/console");
//        } else {
//            view = new ModelAndView("login");
//        }
        return view;
    }

    @RequestMapping("/logout")
    public ModelAndView logOut(HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        ModelAndView view = new ModelAndView("redirect:/");
        session.removeAttribute(Config.SESSION_USERNAME_KEY);
        return view;
    }
}
