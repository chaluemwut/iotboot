package nsc.iot.controller;

import nsc.iot.Config;
import nsc.iot.gateway.MQTTAllSub;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.service.UserProfileRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.UserProfile;
import nsc.iot.mqtt.MQTTPub;
import nsc.iot.sec.IOTEncrypt;
import nsc.iot.sec.PBKDF2PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/registor")
public class Registor extends BaseController {
    @Autowired
    private PBKDF2PasswordEncoder passwordEncoder;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @Autowired
    private UserProfileRespository userProfileRespository;

    @Autowired
    private IOTUserRespository iotUserRespository;

    @Autowired
    private IOTEncrypt iotEncrypt;

    @Autowired
    private MQTTAllSub mqttAllSub;


    @RequestMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("registor");
        return  view;
    }

    @RequestMapping("/action")
    public ModelAndView action(@RequestParam String username,
                               @RequestParam(name = "password1") String password,
                               @RequestParam(name = "password2") String password2,
                               @RequestParam(name = "full_username", defaultValue = "") String fullName,
                               HttpServletRequest request){
        ModelAndView view;

        UserProfile user = userProfileRespository.findByUserName(username);
        if(!password.equals(password2)){
            view = new ModelAndView("redirect:/registor");
            view.addObject("error","user");
            view.addObject("message", "Password not match");
            return view;
        }

        if(password == null || "".equals(password)){
            view = new ModelAndView("redirect:/registor");
            view.addObject("error","user");
            view.addObject("message", "Password can not empty");
            return view;
        }

        if(username == null || "".equals(username)){
            view = new ModelAndView("redirect:/registor");
            view.addObject("error","user");
            view.addObject("message", "Username can not empty");
            return view;
        }

        if(user != null){
            view = new ModelAndView("redirect:/registor");
            view.addObject("error","user");
            view.addObject("message", "User exist");
        } else {
            view = new ModelAndView("redirect:/console");

//          add mqtt user
            String pw = passwordEncoder.encode(password);
            UserProfile userProfile = new UserProfile();
            userProfile.setUserName(username);
            userProfile.setPassWord(pw);
            userProfile.setSuperFlag(1);
            userProfile = userProfileRespository.save(userProfile);

            IOTUser iotUser = new IOTUser();
            iotUser.setToken(iotEncrypt.encrypt(password));
            iotUser.setUserProfileId(userProfile.getId());
//            iotUser.setUserProfile(userProfile);
            iotUser.setFullName(fullName);
            iotUser = iotUserRespository.save(iotUser);
            Object [] iotMQTTUser = new Object[3];
            iotMQTTUser[0] = iotUser.getId();
            iotMQTTUser[1] = iotUser.getToken();
            iotMQTTUser[2] = userProfile.getUserName();
            mqttAllSub.mqttSubTopic(iotMQTTUser);

            authenticateUserAndSetSession(username, password, request, userProfile);
        }
        getSpringCache().clear();
        return view;
    }

    private void authenticateUserAndSetSession(String username, String password, HttpServletRequest request, UserProfile userProfile) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        HttpSession session = request.getSession();
        session.setAttribute(Config.SESSION_USERNAME_KEY, userProfile);

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationManager.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }
}
