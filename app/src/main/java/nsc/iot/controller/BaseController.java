package nsc.iot.controller;

import nsc.iot.cache.SpringCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseController {
    @Autowired
    private SpringCache springCache;

    public SpringCache getSpringCache() {
        return springCache;
    }
}
