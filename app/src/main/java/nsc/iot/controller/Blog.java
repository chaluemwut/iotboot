package nsc.iot.controller;

import nsc.iot.Config;
import nsc.iot.bean.IOTBlogFeed;
import nsc.iot.model.service.BlogStatusRespository;
import nsc.iot.model.service.IOTBlogAnswerRespository;
import nsc.iot.model.service.IOTBlogRespository;
import nsc.iot.model.service.IOTUserRespository;
import nsc.iot.model.table.BlogStatus;
import nsc.iot.model.table.IOTBlog;
import nsc.iot.model.table.IOTBlogAnswer;
import nsc.iot.model.table.IOTUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/blog")
public class Blog extends BaseController {
    @Autowired
    private IOTBlogRespository iotBlogRespository;

    @Autowired
    private IOTUserRespository iotUserRespository;

    @Autowired
    private IOTBlogAnswerRespository iotBlogAnswerRespository;

    @Autowired
    private BlogStatusRespository blogStatusRespository;

    @Autowired
    HttpSession session;

    @RequestMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("blog/index");
        IOTUser secIOTUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
        Set<IOTBlog> blogList = iotBlogRespository.findAllByOrderByCreateDateDesc();
        List<IOTBlogFeed> newFeed = new ArrayList<>();
        blogList.forEach(e->{
            String headerImg = null;
            try {
                IOTBlog iotBlog = e;
                String strMessage = iotBlog.getMessage();
                int i = strMessage.indexOf("<img src=\"data:image/png") + 10;
                String newStr = strMessage.substring(i);
                int last = newStr.indexOf(">") - 1;
                headerImg = newStr.substring(0, last);
            } catch (Exception ex){

            }
            IOTBlogFeed blogFeed = new IOTBlogFeed();
            blogFeed.setIotBlog(e);
            blogFeed.setTitleImage(headerImg);
            newFeed.add(blogFeed);
        });

        view.addObject("blogs", newFeed);
        return view;
    }

    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView view = new ModelAndView("blog/list");
        IOTUser secIOTUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
//        secIOTUser.getUserProfileId()
        Set<IOTBlog> blogList = iotBlogRespository.findByUserNameOrderByCreateDateDesc(secIOTUser.getUserProfileId());
        view.addObject("blogs", blogList);
        return view;
    }

    @RequestMapping("/new")
    public ModelAndView newBlog(){
        ModelAndView view = new ModelAndView("blog/new");
        IOTBlog blog = new IOTBlog();
        blog.setId(-1);
        view.addObject("blog", blog);
        getSpringCache().clear();
        return view;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView create(@RequestParam String title,
                               @RequestParam String message,
                               @RequestParam String action,
                               @RequestParam int id){
        IOTUser secIOTUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
        int blogStatus = 1;

        if("save".equals(action)){
            blogStatus = 1;
        } else {
            blogStatus = 2;
        }

        if(id == -1) {
            IOTBlog iotBlog = new IOTBlog();
            iotBlog.setTitle(title);
            iotBlog.setMessage(message);
            iotBlog.setIotUserId(secIOTUser.getId());
            iotBlog.setBlogStatusId(blogStatus);
            iotBlogRespository.save(iotBlog);
        } else {
            Optional<IOTBlog> blog = iotBlogRespository.findById(id);
            if(blog.isPresent()){
                IOTBlog updateBlog = blog.get();
                updateBlog.setTitle(title);
                updateBlog.setMessage(message);
                updateBlog.setBlogStatusId(blogStatus);
                iotBlogRespository.save(updateBlog);
            }
        }

        ModelAndView view = new ModelAndView("redirect:/blog/list");
        getSpringCache().clear();
        return view;
    }

    @RequestMapping("/detail")
    public ModelAndView detail(@RequestParam Integer id){
        ModelAndView view = new ModelAndView("blog/new");
        Optional<IOTBlog> blogObj = iotBlogRespository.findById(id);
        IOTBlog blog = (IOTBlog) blogObj.get();
        view.addObject("blog", blog);
        return view;
    }

    @RequestMapping("/view")
    public ModelAndView view(@RequestParam int id){
        ModelAndView view = new ModelAndView("blog/view");
        Optional<IOTBlog> iotBlog = iotBlogRespository.findById(id);
        List<IOTBlogAnswer> answers = iotBlogAnswerRespository.findByBlog(iotBlog.get().getId());
        List<String> ansList = new ArrayList<>();
        answers.forEach(e->{
//            String str = null;
//            if(e.getIotUser() == null){
//                str = "Anonymous : "+e.getMessage();
//            } else {
//                str = e.getIotUser().getUserProfile().getUserName()+" : "+e.getMessage();
//            }
            ansList.add("Anonymous");
        });
        view.addObject("blog", iotBlog.get());
        view.addObject("anslist", ansList);
        return  view;
    }


    @RequestMapping("/comment")
    public ModelAndView comment(@RequestParam int id,
                                @RequestParam(value = "comment_message") String comment){
        ModelAndView view = new ModelAndView("redirect:/blog/view?id="+id);
        IOTUser iotUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
        Optional<IOTBlog> blog = iotBlogRespository.findById(id);

        IOTBlogAnswer iotBlogAnswer = new IOTBlogAnswer();
        iotBlogAnswer.setMessage(comment);
//        iotBlogAnswer.setIotBlog(blog.get());
        iotBlogAnswer.setIotBlogId(blog.get().getId());
        iotBlogAnswer.setIotUserId(iotUser.getId());
        iotBlogAnswerRespository.save(iotBlogAnswer);
        getSpringCache().clear();
        return view;
    }
}
