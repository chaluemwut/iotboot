package nsc.iot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/classroom")
public class ClassRoom {
    private static final String baseFolder = "classroom/%s";

    @RequestMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView(String.format(baseFolder, "index"));
        return view;
    }

}
