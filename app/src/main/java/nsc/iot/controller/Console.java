package nsc.iot.controller;

import nsc.iot.Config;
import nsc.iot.model.service.MQTTDataRespository;
import nsc.iot.model.service.UserProfileRespository;
import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.UserProfile;
import nsc.iot.mqtt.MQTTPub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/console")
public class Console extends BaseController {
    @Autowired
    HttpSession session;

    @Autowired
    MQTTDataRespository mqttDataRespository;

    @Autowired
    MQTTPub mqttPub;

    @Autowired
    private UserProfileRespository userProfileRespository;


    class ChartData {
        private Object createDate;
        private Object value;

        public Object getCreateDate() {
            return createDate;
        }

        public void setCreateDate(Object createDate) {
            this.createDate = createDate;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }

    @RequestMapping
    public ModelAndView index(@RequestParam(name = "step", required = false) String step) {
        ModelAndView view = new ModelAndView("console/index");
        IOTUser iotUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);

//        Object[] dataList = mqttDataRespository.coundByTopic(iotUser.getId());
//        Object[] consoleList = mqttDataRespository.countByDate(iotUser.getId());
//        List<ChartData> chartList = new ArrayList<>();
//        for(int i=0; i<consoleList.length; i++){
//            Object[] chartDataList = (Object[]) consoleList[i];
//            ChartData chartData = new ChartData();
//            chartData.setCreateDate(chartDataList[0]);
//            chartData.setValue(chartDataList[1]);
//            chartList.add(chartData);
//        }
//
//        if(chartList.size() > 0) {
//            ChartData lastDate = chartList.get(chartList.size() - 1);
//            SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
//            String firstStringDate = "";
//            try {
//                Date firstDate = format.parse(lastDate.getCreateDate().toString());
//                Calendar cal = Calendar.getInstance();
//                cal.setTime(firstDate);
//                cal.add(Calendar.DATE, -30);
//                Date firstResultDate = cal.getTime();
//                firstStringDate = format.format(firstResultDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            view.addObject("mqtt_list", dataList);
//            view.addObject("chart_list", chartList);
//            view.addObject("last_date", lastDate.getCreateDate());
//            view.addObject("first_date", firstStringDate);
//        }

        Optional<UserProfile> userProfile = userProfileRespository.findById(iotUser.getUserProfileId());
        if(userProfile.isPresent()){
            UserProfile userProfileData = userProfile.get();
            view.addObject("user_name", userProfileData.getUserName());
        }

        view.addObject("step", step);
        return view;
    }

    @RequestMapping("pub")
    public ModelAndView mqttPub(@RequestParam String topic,
                                @RequestParam String message,
                                @RequestParam String step){
        ModelAndView view = new ModelAndView("redirect:/console?step="+step);
        System.out.println("start pub message");
        IOTUser iotUser = (IOTUser) session.getAttribute(Config.SESSION_USERNAME_KEY);
        System.out.println("public user : "+iotUser);
        mqttPub.pub(topic, message);
        return  view;
    }

}
