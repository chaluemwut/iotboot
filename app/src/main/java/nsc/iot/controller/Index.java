package nsc.iot.controller;

import nsc.iot.bean.IOTBlogFeed;
import nsc.iot.model.service.BlogStatusRespository;
import nsc.iot.model.service.IOTBlogRespository;
import nsc.iot.model.table.IOTBlog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class Index {
    @Autowired
    private IOTBlogRespository iotBlogRespository;

    @Autowired
    private BlogStatusRespository blogStatusRespository;

    @RequestMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("index");
        Iterable<IOTBlog> blogs = iotBlogRespository.findPublishBlog();
        List<IOTBlogFeed> newFeed = new ArrayList<>();

        blogs.forEach(e->{
            String headerImg = null;
            try {
                IOTBlog iotBlog = e;
                String strMessage = iotBlog.getMessage();
                headerImg = headerImage(strMessage);
            } catch (Exception ex){

            }
            IOTBlogFeed blogFeed = new IOTBlogFeed();
            blogFeed.setIotBlog(e);
            blogFeed.setTitleImage(headerImg);
            newFeed.add(blogFeed);
        });

        view.addObject("blogs", newFeed);
        return view;
    }

    @Cacheable(value = "headerImage", key = "#allImage")
    private String headerImage(String allImage){
        int i = allImage.indexOf("<img src=\"data:image/png") + 10;
        String newStr = allImage.substring(i);
        int last = newStr.indexOf(">") - 1;
        return newStr.substring(0, last);
    }
}
