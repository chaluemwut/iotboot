package nsc.iot.model.service;

import nsc.iot.model.table.UserProfile;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

public interface UserProfileRespository extends CrudRepository<UserProfile, Integer> {

    @Cacheable("UserProfileRespository_findByUserName")
    UserProfile findByUserName(String userName);

}
