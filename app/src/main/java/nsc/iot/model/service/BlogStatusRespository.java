package nsc.iot.model.service;

import nsc.iot.model.table.BlogStatus;
import org.springframework.data.repository.CrudRepository;

public interface BlogStatusRespository extends CrudRepository<BlogStatus, Integer> {

}
