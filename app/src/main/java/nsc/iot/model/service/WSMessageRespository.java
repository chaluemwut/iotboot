package nsc.iot.model.service;

import nsc.iot.model.table.WSMessage;
import nsc.iot.model.table.WSTopic;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface WSMessageRespository extends CrudRepository<WSMessage, Integer> {

}
