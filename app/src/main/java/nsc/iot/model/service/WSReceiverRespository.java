package nsc.iot.model.service;

import nsc.iot.model.table.WSReceiver;
import org.springframework.data.repository.CrudRepository;

public interface WSReceiverRespository extends CrudRepository<WSReceiver, Integer> {

    WSReceiver findByClientId(String clientId);

}
