package nsc.iot.model.service;

import nsc.iot.model.table.MQTTData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MQTTDataRespository extends CrudRepository<MQTTData, Integer> {

    @Cacheable("MQTTDataRespository_coundByTopic")
    @Query(value = "select topic, count(message) " +
            " from mqtt_data " +
            " where iot_user_id = :iotUserId" +
            " group by topic ", nativeQuery = true)
    Object[] coundByTopic(@Param("iotUserId") int iotUserId);


    @Cacheable("MQTTDataRespository_countByDate")
    @Query(value = "select date(create_date), count(1) " +
            " from mqtt_data " +
            " where iot_user_id = :iotUserId " +
            " group by date(create_date) " +
            " order by date(create_date) asc ", nativeQuery = true)
    Object [] countByDate(@Param("iotUserId") int iotUserId);

}
