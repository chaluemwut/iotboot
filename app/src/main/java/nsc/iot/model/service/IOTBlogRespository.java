package nsc.iot.model.service;

import nsc.iot.model.table.BlogStatus;
import nsc.iot.model.table.IOTBlog;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface IOTBlogRespository extends CrudRepository<IOTBlog, Integer> {

    @Cacheable("IOTBlogRespository_findByUserNameOrderByCreateDateDesc")
    @Query(value = "select a.* " +
            " from iot_blog a, " +
            "      iot_user b," +
            "      users c " +
            " where a.iot_user_id = b.id " +
            "      and b.user_profile_id = c.id " +
            "      and c.user_name = :username " +
            " order by a.create_date desc ", nativeQuery = true)
    Set<IOTBlog> findByUserNameOrderByCreateDateDesc(@Param("username") String username);

    @Cacheable("IOTBlogRespository_findByUserNameOrderByCreateDateDesc")
    @Query(value = "select a.* " +
            " from iot_blog a, " +
            "      iot_user b," +
            "      users c " +
            " where a.iot_user_id = b.id " +
            "      and b.user_profile_id = c.id " +
            "      and b.user_profile_id = :user_profile_id " +
            " order by a.create_date desc", nativeQuery = true)
    Set<IOTBlog> findByUserNameOrderByCreateDateDesc(@Param("user_profile_id") int userProfileId);

    @Cacheable("IOTBlogRespository_findPublishBlog")
    @Query(value = "select * " +
            " from iot_blog " +
            " where blog_status_id = 2", nativeQuery = true)
    Set<IOTBlog> findPublishBlog();

//    @Query("select a from IOTBlog a where a.blogStatus.id = 2 order by a.createDate desc")
    @Cacheable("IOTBlogRespository_findPublishBlog")
    @Query(value = "select a.* " +
            " from iot_blog a" +
            " where a.blog_status_id = 2 " +
            " order by a.create_date desc", nativeQuery = true)
    Set<IOTBlog> findAllByOrderByCreateDateDesc();

}
