package nsc.iot.model.service;

import nsc.iot.model.table.IOTUser;
import nsc.iot.model.table.UserProfile;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface IOTUserRespository extends CrudRepository<IOTUser, Integer> {

//    @Query("select a from IOTUser a where a.userProfile.userName = :userName")
//    @Query(value = "select a.* " +
//            " from iot_user a " +
//            "     user_profile b " +
//            " where a.user_profile_id = b.id " +
//            "      b.username = :username ", nativeQuery = true)
//    IOTUser findByUserName(@Param("userName") String userName);

    @Cacheable("IOTUserRespository_findIoTUserByUserName")
    @Query(value = "select a.* " +
            " from iot_user a, users b" +
            " where a.user_profile_id = b.id" +
            "       and b.username = :username ", nativeQuery = true)
    IOTUser findIoTUserByUserName(@Param("username") String userName);

    @Cacheable("IOTUserRespository_findPWByUserName")
    @Query(value = "select b.pw " +
            " from iot_user a, users b" +
            " where a.user_profile_id = b.id " +
            "       and b.username = :username  ", nativeQuery = true)
    String findPWByUserName(@Param("username") String userName);

    @Cacheable("IOTUserRespository_findAllIotUser")
    @Query(value = "select distinct a.id, a.token, b.username " +
            " from iot_user a, users b " +
            " where a.user_profile_id = b.id", nativeQuery = true)
    Set<Object[]> findAllIotUser();

    @Cacheable("IOTUserRespository_findUserName")
    @Query(value = "select distinct b.username " +
            " from iot_user a, users b " +
            " where a.user_profile_id = b.id " +
            "       and a.id = :iot_user_id", nativeQuery = true)
    String findUserName(@Param("iot_user_id") int iotUserId);


//    IOTUser findByWsClientId(String wsClientId);

}
