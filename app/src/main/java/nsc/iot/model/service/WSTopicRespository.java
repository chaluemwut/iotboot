package nsc.iot.model.service;

import nsc.iot.model.table.WSTopic;
import org.springframework.data.repository.CrudRepository;

public interface WSTopicRespository extends CrudRepository<WSTopic, Integer> {

    WSTopic findByTopic(String topic);

}
