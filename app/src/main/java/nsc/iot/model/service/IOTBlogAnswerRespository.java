package nsc.iot.model.service;

import nsc.iot.model.table.IOTBlogAnswer;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IOTBlogAnswerRespository extends CrudRepository<IOTBlogAnswer, Integer> {


    @Cacheable("IOTBlogAnswerRespository_findByBlog")
    @Query(value = "select * from iot_blog_answer where iot_user_id = :id order by create_date desc", nativeQuery = true)
    List<IOTBlogAnswer> findByBlog(@Param("id") int id);

}
