package nsc.iot.model.table;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "iot_blog_answer")
public class IOTBlogAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(length = 2_000)
    private String message;

//    @ManyToOne
//    private IOTBlog iotBlog;

//    @OneToOne
//    private IOTUser iotUser;

    @Column(name = "iot_blog_id")
    private int iotBlogId;

    @Column(name = "iot_user_id")
    private int iotUserId;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getIotBlogId() {
        return iotBlogId;
    }

    public void setIotBlogId(int iotBlogId) {
        this.iotBlogId = iotBlogId;
    }

    public int getIotUserId() {
        return iotUserId;
    }

    public void setIotUserId(int iotUserId) {
        this.iotUserId = iotUserId;
    }
}
