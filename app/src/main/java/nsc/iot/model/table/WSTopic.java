package nsc.iot.model.table;


import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ws_topic")
public class WSTopic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String topic;

    @OneToOne
    @JoinColumn(name = "iot_user_id")
    private IOTUser publisher;

    @ManyToMany
    private Set<WSReceiver> wsReceivers;

    @OneToMany(mappedBy = "wsTopic")
    private Set<WSMessage> wsMessage;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public IOTUser getPublisher() {
        return publisher;
    }

    public void setPublisher(IOTUser publisher) {
        this.publisher = publisher;
    }

    public Set<WSReceiver> getWsReceivers() {
        return wsReceivers;
    }

    public void setWsReceivers(Set<WSReceiver> wsReceivers) {
        this.wsReceivers = wsReceivers;
    }

    public Set<WSMessage> getWsMessage() {
        return wsMessage;
    }

    public void setWsMessage(Set<WSMessage> wsMessage) {
        this.wsMessage = wsMessage;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
