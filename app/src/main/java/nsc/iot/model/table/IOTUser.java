package nsc.iot.model.table;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "iot_user")
public class IOTUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "user_profile_id")
    private int userProfileId;

    @Column(name = "token")
    private String token;

    @Column(name = "ws_client_id")
    private String wsClientId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getWsClientId() {
        return wsClientId;
    }

    public void setWsClientId(String wsClientId) {
        this.wsClientId = wsClientId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public int getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(int userProfileId) {
        this.userProfileId = userProfileId;
    }


    @Override
    public String toString() {
        return "IOTUser{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", userProfileId=" + userProfileId +
                ", token='" + token + '\'' +
                ", wsClientId='" + wsClientId + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
