package nsc.iot.model.table;

import nsc.iot.controller.Blog;

import javax.persistence.*;

@Entity
@Table(name = "blog_status")
public class BlogStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String name;

//    @Column(name = "iot_blog_id")
//    private int iotBlogId;

//    @OneToOne(mappedBy = "blogStatus")
//    private IOTBlog iotBlog;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public int getIotBlogId() {
//        return iotBlogId;
//    }
//
//    public void setIotBlogId(int iotBlogId) {
//        this.iotBlogId = iotBlogId;
//    }
}
