package nsc.iot.model.table;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ws_message")
public class WSMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String message;

    @ManyToOne
    private IOTUser iotUser;

    @ManyToOne
    private WSTopic wsTopic;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IOTUser getIotUser() {
        return iotUser;
    }

    public void setIotUser(IOTUser iotUser) {
        this.iotUser = iotUser;
    }

    public WSTopic getWsTopic() {
        return wsTopic;
    }

    public void setWsTopic(WSTopic wsTopic) {
        this.wsTopic = wsTopic;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
