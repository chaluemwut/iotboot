package nsc.iot.model.table;


import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "iot_blog")
public class IOTBlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

//    @OneToOne
//    private IOTUser iotUser;

    @Column(name = "iot_user_id")
    private int iotUserId;

    @Column
    private String title;

    @Column(columnDefinition = "longtext")
    private String message;

    //1 save
    //2 publish
//    @OneToOne
//    @JoinColumn(name = "status_id")
//    private BlogStatus blogStatus;

    @Column(name = "blog_status_id")
    private int blogStatusId;

//    @OneToMany(mappedBy = "iotBlog")
//    private Set<IOTBlogAnswer> iotBlogAnswerSet;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIotUserId() {
        return iotUserId;
    }

    public void setIotUserId(int iotUserId) {
        this.iotUserId = iotUserId;
    }

    public int getBlogStatusId() {
        return blogStatusId;
    }

    public void setBlogStatusId(int blogStatusId) {
        this.blogStatusId = blogStatusId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
