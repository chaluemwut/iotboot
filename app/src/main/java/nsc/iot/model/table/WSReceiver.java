package nsc.iot.model.table;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ws_receiver")
public class WSReceiver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "clien_id")
    private String clientId;

    @ManyToOne
    private IOTUser iotUser;

    @ManyToMany(mappedBy = "wsReceivers")
    private Set<WSTopic> wsTopicSet;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public IOTUser getIotUser() {
        return iotUser;
    }

    public void setIotUser(IOTUser iotUser) {
        this.iotUser = iotUser;
    }

    public Set<WSTopic> getWsTopicSet() {
        return wsTopicSet;
    }

    public void setWsTopicSet(Set<WSTopic> wsTopicSet) {
        this.wsTopicSet = wsTopicSet;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
