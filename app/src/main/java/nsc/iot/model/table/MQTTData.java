package nsc.iot.model.table;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mqtt_data")
public class MQTTData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String topic;

    @Column
    private String message;

    @Column(name = "iot_user_id")
    private int iotUserId;

//    @ManyToOne(fetch = FetchType.EAGER)
//    private IOTUser iotUser;

    @Column(name = "create_date")
    private Date createDate = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getIotUserId() {
        return iotUserId;
    }

    public void setIotUserId(int iotUserId) {
        this.iotUserId = iotUserId;
    }
}
