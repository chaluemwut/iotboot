package nsc.iot.system;

import nsc.iot.Config;
import nsc.iot.gateway.MQTTAllSub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class InitContext {

    @Autowired
    private MQTTAllSub mqttAllSub;

    @EventListener({ContextRefreshedEvent.class})
    public void contextStart(){
//        if(Config.MQTT_DEMON) {
//            mqttAllSub.start();
//        }
//        System.out.println("start init context");
//        System.out.println("start mqtt sub");
//        mqttAllSub.start();
//        System.out.println("end init context");
    }
}
