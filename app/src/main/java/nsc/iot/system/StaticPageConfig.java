package nsc.iot.system;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class StaticPageConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/info/iot").setViewName("info/iot");
        registry.addViewController("/info/iot_protocol").setViewName("info/iot_protocol");
        registry.addViewController("/info/mqtt").setViewName("info/mqtt");
        registry.addViewController("/info/support_mqtt").setViewName("info/support_mqtt");
        registry.addViewController("/info/support_websocket").setViewName("info/support_websocket");

        registry.addViewController("/info/iot_device").setViewName("info/iot_device");

        registry.addViewController("/aboutus").setViewName("aboutus");

        registry.addViewController("/classroom/room/ch1").setViewName("classroom/room/ch1");
        registry.addViewController("/classroom/room/ch2").setViewName("classroom/room/ch2");
        registry.addViewController("/classroom/room/ch3").setViewName("classroom/room/ch3");
        registry.addViewController("/classroom/room/maven").setViewName("classroom/room/maven");
        registry.addViewController("/classroom/room/git").setViewName("classroom/room/git");

    }

}