INSERT INTO users (id, username, pw, super) VALUES (1, 'jjolie', 'PBKDF2$sha256$901$pN94c3+KCcNvIV1v$LWEyzG6v/gtvTrjx551sNcWWfwIZKAg0', 0);
INSERT INTO users (id, username, pw, super) VALUES (2, 'a', 'PBKDF2$sha256$901$XPkOwNbd05p5XsUn$1uPtR6hMKBedWE44nqdVg+2NPKvyGst8', 0);
INSERT INTO users (id, username, pw, super)
	VALUES (3, 'su1',
	'PBKDF2$sha256$901$chEZ4HcSmKtlV0kf$yRh2N62uq6cHoAB6FIrxIN2iihYqNIJp',
	1);
INSERT INTO users (id, username, pw, super)
	VALUES (4, 'S1',
	'PBKDF2$sha256$901$sdMgoJD3GaRlTF7y$D7Krjx14Wk745bH36KBzVwHwRQg0a+z6',
	1);
INSERT INTO users (id, username, pw, super)
	VALUES (5, 'm1',
	'PBKDF2$sha256$2$NLu+mJ3GwOpS7JLk$eITPuWG/+WMf6F3bhsT5YlYPY6MmJHvM',
	0);
-- PSK
INSERT INTO users (id, username, pw, super)
	VALUES (6, 'ps1',
	'deaddead',
	0);

insert into users (id, username, pw, super) value (7, 'off','PBKDF2$sha256$901$YTuoUq33GlEZ2zRM$2YKM336mCtlDTGagoq2JG04j1zGKAise', 1);
insert into users (id, username, pw, super) value (8, 'chaluemwut','PBKDF2$sha256$901$FlxCSh0sYJrEdhLb$PEHgesGgfRU6uO+yI+Q5of5eWMeuOo9v', 1);

insert into iot_user (token, user_profile_id, full_name, create_date) value ('d13GYtj0nEA=', 7, 'off', now());
insert into iot_user (token, user_profile_id, full_name, create_date) value ('28gjvzVcfu/TsXa01bcc4w==', 8, 'Chaluemwut Noyunsan', now());

--CREATE UNIQUE INDEX acls_user_topic ON acls (username, topic(228));

INSERT INTO acls (username, topic, rw) VALUES ('jjolie', 'loc/jjolie', 1);
INSERT INTO acls (username, topic, rw) VALUES ('jjolie', 'loc/ro', 1);
INSERT INTO acls (username, topic, rw) VALUES ('jjolie', 'loc/rw', 2);
INSERT INTO acls (username, topic, rw) VALUES ('jjolie', '$SYS/something', 1);
INSERT INTO acls (username, topic, rw) VALUES ('a', 'loc/test/#', 1);
INSERT INTO acls (username, topic, rw) VALUES ('a', '$SYS/broker/log/+', 1);
INSERT INTO acls (username, topic, rw) VALUES ('su1', 'mega/secret', 1);
INSERT INTO acls (username, topic, rw) VALUES ('nop', 'mega/secret', 1);

INSERT INTO acls (username, topic, rw) VALUES ('jog', 'loc/#', 1);
INSERT INTO acls (username, topic, rw) VALUES ('m1', 'loc/#', 1);

INSERT INTO acls (username, topic, rw) VALUES ('ps1', 'x', 1);
INSERT INTO acls (username, topic, rw) VALUES ('ps1', 'blabla/%c/priv/#', 1);

--IOT initialzer data
insert into blog_status (id, name) value (1, 'draft');
insert into blog_status (id, name) value (2, 'publish');