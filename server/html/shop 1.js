(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// stage content:
(lib.shop1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shoppingonline32();

	this.instance_1 = new lib.shoppingonline33();

	this.instance_2 = new lib.shoppingonline32();
	this.instance_2.setTransform(0,0.1);

	this.instance_3 = new lib.shoppingonline34();

	this.instance_4 = new lib.shoppingonline35();

	this.instance_5 = new lib.shoppingonline36();

	this.instance_6 = new lib.shoppingonline37();

	this.instance_7 = new lib.shoppingonline38();

	this.instance_8 = new lib.shoppingonline31();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance,p:{y:0}}]}).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1}]},10).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3}]},14).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3},{t:this.instance_4}]},15).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3},{t:this.instance_4},{t:this.instance_5}]},15).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3},{t:this.instance_4},{t:this.instance_5},{t:this.instance_6}]},15).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3},{t:this.instance_4},{t:this.instance_5},{t:this.instance_6},{t:this.instance_7}]},15).to({state:[{t:this.instance,p:{y:0.1}},{t:this.instance_1},{t:this.instance_3},{t:this.instance_4},{t:this.instance_5},{t:this.instance_6},{t:this.instance_7},{t:this.instance_8}]},15).wait(36));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,915,384.1);


// symbols:
(lib.shoppingonline31 = function() {
	this.initialize(img.shoppingonline31);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline32 = function() {
	this.initialize(img.shoppingonline32);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline33 = function() {
	this.initialize(img.shoppingonline33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline34 = function() {
	this.initialize(img.shoppingonline34);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline35 = function() {
	this.initialize(img.shoppingonline35);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline36 = function() {
	this.initialize(img.shoppingonline36);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline37 = function() {
	this.initialize(img.shoppingonline37);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);


(lib.shoppingonline38 = function() {
	this.initialize(img.shoppingonline38);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,384);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;