/*
 *  HTTP over TLS (HTTPS) example sketch
 *
 *  This example demonstrates how to use
 *  WiFiClientSecure class to access HTTPS API.
 *  We fetch and display the status of
 *  esp8266/Arduino project continuous integration
 *  build.
 *
 *  Created by Ivan Grokhotkov, 2015.
 *  This example is in public domain.
 */

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

//char* ssid = "ce";
//char* password = "aabbccbbaa";
//
//const char* host = "iotboot.com";
//const int httpsPort = 443;

// Use web browser to view and copy
// SHA1 fingerprint of the certificate
const char* fingerprint = "90 63 13 FB 9F 73 A6 49 4C 99 FF 19 BD 7F 67 74 16 42 37 CB";

WiFiClientSecure client;

void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.print("connecting to ");
//  WiFi.mode(WIFI_STA);
//  WiFi.begin(ssid, password);
  WiFi.begin("ComEng.WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Use WiFiClientSecure class to create TLS connection  
  Serial.print("connecting to ");
//  Serial.println(host);
  if (!client.connect("httpbin.org", 443)) {
    Serial.println("connection failed");
    return;
  }

  if (client.verify(fingerprint, "httpbin.org")) {
    Serial.println("certificate matches");
  } else {
    Serial.println("certificate doesn't match");
  }

  String url = "/get?show_env=1";
  Serial.print("requesting URL: ");
  Serial.println(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: httpbin.org\r\n" +
               "Connection: close\r\n\r\n");

  Serial.println("request sent");
  while (client.connected()) {
    String line = client.readStringUntil('\n');
//    if (line == "\r") {
//      Serial.println("headers received");
//      break;
//    }
    Serial.println(line);
  }
  
}

void loop() {
//  if (client.available()) {
//    char c = client.read();
//    Serial.print(c);
//  }
}
