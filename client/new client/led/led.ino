#include <ESP8266WiFi.h>
#include <MQTTClient.h>

WiFiClient net;
MQTTClient client;

void setup() {
  Serial.begin(9600);
  Serial.println("start program");
  pinMode(16, OUTPUT);

  WiFi.begin("net","12345678");

  client.begin("iotboot.com", net);
  client.onMessage(messageReceived);

  client.connect("clienta","off", "1234");
  client.subscribe("hello");
}

void loop() {
  client.loop();
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  Serial.println(payload);
  if(topic == "hello" && payload == "on"){    
    digitalWrite(16, HIGH);
  } else {
    digitalWrite(16, LOW);
  }
}
