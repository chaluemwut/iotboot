/*
 *  HTTP over TLS (HTTPS) example sketch
 *
 *  This example demonstrates how to use
 *  WiFiClientSecure class to access HTTPS API.
 *  We fetch and display the status of
 *  esp8266/Arduino project continuous integration
 *  build.
 *
 *  Created by Ivan Grokhotkov, 2015.
 *  This example is in public domain.
 */

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>

char* ssid = "ce";
char* password = "aabbccbbaa";

const char* host = "iot.eclipse.org";
const int httpsPort = 1883;

// Use web browser to view and copy
// SHA1 fingerprint of the certificate
//const char* fingerprint = "48 F4 79 4E 5E 00 CC 09 77 85 34 0A 5F 09 FA AC CE 69 D4 00";

//WiFiClientSecure client;

WiFiClientSecure ethClient;
PubSubClient client(ethClient);

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("aaa")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.subscribe("hello");      
//      client.publish("outTopic","hello world");
      // ... and resubscribe      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
//  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Use WiFiClientSecure class to create TLS connection  
  Serial.print("connecting to ");

  client.setServer(host, 1883);
  client.setCallback(callback);
  client.connect("aaa");
//  client.subscribe("hello");
//  client.publish("hello", "xyz");  
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
