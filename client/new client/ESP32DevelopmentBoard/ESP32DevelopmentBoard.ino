// This example uses an ESP32 Development Board
// to connect to shiftr.io.
//
// You can check on your device after a successful
// connection here: https://shiftr.io/try.
//
// by Joël Gähwiler
// https://github.com/256dpi/arduino-mqtt

#include <ESP8266WiFi.h>
#include <MQTTClient.h>

WiFiClient net;
MQTTClient client;

void setup() {  
  Serial.begin(9600);
  Serial.println("start program");
  WiFi.begin("ComEng.WiFi", "");

  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported by Arduino.
  // You need to set the IP address directly.
  client.begin("iotboot.com", net);
  client.onMessage(messageReceived);    
  connect();  
}

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!client.connect("clienta", "off","1234")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!"); 
  client.subscribe("hello");
  
}

void loop() {
  client.loop();
//  delay(10);  // <- fixes some issues with WiFi stability
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}
