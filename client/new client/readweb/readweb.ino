#include <ESP8266WiFi.h>

WiFiClientSecure client;

void setup() {
  Serial.begin(9600);
  Serial.println("start program");
  WiFi.begin("net", "12345678");
  if(!client.connect("httpbin.org",443)){
    Serial.println("Connect failed");
    return;
  }
  Serial.println("Connect successed");

  if (client.verify("90 63 13 FB 9F 73 A6 49 4C 99 FF 19 BD 7F 67 74 16 42 37 CB", "httpbin.org")) {
    Serial.println("certificate matches");
  } else {
    Serial.println("certificate doesn't match");
  }
  
  client.print("GET /get?show_env=1 HTTP/1.1\r\nHost: httpbin.org\r\nConnection: close\r\n\r\n");

  Serial.println("Start result");
  while(client.connected()){
    String line = client.readStringUntil('\n');
    Serial.println(line);
  }
  Serial.println("end result");
}

void loop() {
  // put your main code here, to run repeatedly:

}
