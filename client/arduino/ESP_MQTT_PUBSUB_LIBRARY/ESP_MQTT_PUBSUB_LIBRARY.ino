/*
 
 PUBSUB MQTT library
 
 
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

  Code from Andreas Spiess

*/

#include <PubSubClient.h>
#include <SPI.h>
#include <Ethernet.h>

//unsigned long entry;
//byte clientStatus, prevClientStatus = 99;
//float luminosity, prevLumiosity = -1;
//char valueStr[5];

// create MQTT object
EthernetClient ethClient;
PubSubClient client(ethClient);

byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };

//
void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac);
  delay(100);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(Ethernet.localIP());

  client.setServer("iot.eclipse.org", 1889);
  client.setCallback(callback);
  client.connect("aaa");
  
}

//
void loop() {
  //Serial.println("loop");
  yield();
  if (!client.connected()) {
    Serial.println("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("aaa")) {
      Serial.println("connected");
      // ... and resubscribe
      client.subscribe("hello", 1);
      client.subscribe("hello", 1);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }

//  if (millis() - entry > 1200) {
//    Serial.println("Measure");
//    entry = millis();
//    luminosity = analogRead(A0);
//  }
//  if (client.connected() && prevLumiosity != luminosity) {
//    Serial.println("Publish Luminosity");
//    String hi = (String)luminosity;
//    hi.toCharArray(valueStr, 5);
//    client.publish(USERNAME PREAMBLE T_LUMINOSITY, valueStr);
//    prevLumiosity = luminosity;
//    delay(500);
//  }

//  if (client.connected()&& prevClientStatus != clientStatus ) {
//    Serial.println("Publish Status");
//
//    String hi = (String)clientStatus;
//    hi.toCharArray(valueStr, 2);
//    client.publish(USERNAME PREAMBLE T_CLIENTSTATUS, valueStr);
//    prevClientStatus = clientStatus;
//  }

  client.loop();
}


//----------------------------------------------------------------------


void callback(char* topic, byte * data, unsigned int length) {
  Serial.print(topic);
  Serial.print(": ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)data[i]);
  }
  Serial.println();
}




