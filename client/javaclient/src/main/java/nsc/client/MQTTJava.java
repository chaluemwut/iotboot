package nsc.client;


import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MQTTJava {

    public static void main(String [] args){
        try {
            MqttClient client = new MqttClient("tcp://iot.eclipse.org:1883", "clientb");
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            client.connect(options);

            MqttMessage message = new MqttMessage("off".getBytes());
            client.publish("home/room1", message);
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}

