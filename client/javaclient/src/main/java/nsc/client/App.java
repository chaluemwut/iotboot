package nsc.client;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

//http://my-classes.com/2015/02/05/acl-mosquitto-mqtt-broker-auth-plugin
//mosquitto -c /etc/mosquitto/mosquitto.conf


public class App {

    public static void main( String[] args ) {

        MemoryPersistence persistence = new MemoryPersistence();
        try {
            MqttClient client = new MqttClient("tcp://iotboot.com:1883", "clientb");
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName("off1");
            connOpts.setPassword("1234".toCharArray());
            client.connect(connOpts);

            MqttMessage message = new MqttMessage("off".getBytes());
            client.publish("home/room1", message);
            client.disconnect();
        } catch(MqttException me) {
            me.printStackTrace();
        }
    }
}
