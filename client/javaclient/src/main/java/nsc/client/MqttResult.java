package nsc.client;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttResult implements MqttCallback {

    public void connectionLost(Throwable throwable) {
        System.out.println("connection...");
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println(s+" : "+new String(mqttMessage.getPayload()));
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
