package nsc.myapplication2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("test", "start 123...");

        ((Button)findViewById(R.id.on_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("test", "33333");
                mqttCall("on");
            }
        });

        ((Button)findViewById(R.id.off_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mqttCall("off");
            }
        });

    }

    private void mqttCall(String flag) {
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            MqttClient sampleClient = new MqttClient("tcp://iotboot.com:1883", "clientb", persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName("off");
            connOpts.setPassword("1234".toCharArray());
            sampleClient.connect(connOpts);

            MqttMessage message = new MqttMessage(flag.getBytes());
            sampleClient.publish("hello", message);
            sampleClient.disconnect();
        } catch(MqttException me) {
            me.printStackTrace();
        }
    }
}
