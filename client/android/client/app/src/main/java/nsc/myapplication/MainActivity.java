package nsc.myapplication;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

//import org.eclipse.paho.client.mqttv3.MqttClient;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button)findViewById(R.id.btn_on)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("test", "start");
                MemoryPersistence persistence = new MemoryPersistence();
                try {
                    MqttClient sampleClient = new MqttClient("tcp://iotboot.com:1883", "clientb", persistence);
                    MqttConnectOptions connOpts = new MqttConnectOptions();
                    connOpts.setCleanSession(true);
                    connOpts.setUserName("mosjaruwat");
                    connOpts.setPassword("0868520074".toCharArray());
                    sampleClient.connect(connOpts);

                    MqttMessage message = new MqttMessage("on".getBytes());
                    sampleClient.publish("LED1", message);
                    sampleClient.disconnect();
                } catch(MqttException me) {
                    me.printStackTrace();
                }
                Log.i("test", "end...");
            }
        });


    }
}
