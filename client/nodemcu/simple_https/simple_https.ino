/*
 *  HTTP over TLS (HTTPS) example sketch
 *
 *  This example demonstrates how to use
 *  WiFiClientSecure class to access HTTPS API.
 *  We fetch and display the status of
 *  esp8266/Arduino project continuous integration
 *  build.
 *
 *  Created by Ivan Grokhotkov, 2015.
 *  This example is in public domain.
 */

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

char* ssid = "Sinet_home_wifi";
char* password = "aabbccbbaa";

const char* host = "iotboot.com";
const int httpsPort = 443;

// Use web browser to view and copy
// SHA1 fingerprint of the certificate
const char* fingerprint = "48 F4 79 4E 5E 00 CC 09 77 85 34 0A 5F 09 FA AC CE 69 D4 00";

WiFiClientSecure client;

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Use WiFiClientSecure class to create TLS connection  
  Serial.print("connecting to ");
  Serial.println(host);
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }
  client.print(String("GET ") + "/blog/view?id=1 HTTP/1.1\r\n" +
               "Host: iotboot.com\r\n" +
               "Connection: close\r\n\r\n");
                 
               
  Serial.println("request sent");
}

void loop() {
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }
}
