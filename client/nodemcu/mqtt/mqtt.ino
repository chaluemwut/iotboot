#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

char* ssid = "Sinet_home_wifi";
char* password = "aabbccbbaa";

WiFiClient client;

void setup() {    
  Serial.begin(9600);
  Serial.println("Begin program");
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(WiFi.localIP()); 

  if(client.connect("jsonplaceholder.typicode.com", 80)){
    Serial.println("connected");
    client.println("GET /users/1 HTTP/1.1");
    client.println("Host: jsonplaceholder.typicode.com");
    client.println("Connection: close");
  }
}

void loop() { 
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }
}
